﻿namespace Polarcycle
{
    partial class Staticgraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkBoxHr = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeed = new System.Windows.Forms.CheckBox();
            this.checkBoxCadence = new System.Windows.Forms.CheckBox();
            this.checkBoxAltitude = new System.Windows.Forms.CheckBox();
            this.checkBoxPower = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.checkBoxHr);
            this.flowLayoutPanel1.Controls.Add(this.checkBoxSpeed);
            this.flowLayoutPanel1.Controls.Add(this.checkBoxCadence);
            this.flowLayoutPanel1.Controls.Add(this.checkBoxAltitude);
            this.flowLayoutPanel1.Controls.Add(this.checkBoxPower);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(8);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(937, 41);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // checkBoxHr
            // 
            this.checkBoxHr.AutoSize = true;
            this.checkBoxHr.Checked = true;
            this.checkBoxHr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHr.Location = new System.Drawing.Point(10, 10);
            this.checkBoxHr.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxHr.Name = "checkBoxHr";
            this.checkBoxHr.Size = new System.Drawing.Size(78, 17);
            this.checkBoxHr.TabIndex = 0;
            this.checkBoxHr.Text = "Heart Rate";
            this.checkBoxHr.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeed
            // 
            this.checkBoxSpeed.AutoSize = true;
            this.checkBoxSpeed.Checked = true;
            this.checkBoxSpeed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSpeed.Location = new System.Drawing.Point(92, 10);
            this.checkBoxSpeed.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxSpeed.Name = "checkBoxSpeed";
            this.checkBoxSpeed.Size = new System.Drawing.Size(57, 17);
            this.checkBoxSpeed.TabIndex = 1;
            this.checkBoxSpeed.Text = "Speed";
            this.checkBoxSpeed.UseVisualStyleBackColor = true;
            // 
            // checkBoxCadence
            // 
            this.checkBoxCadence.AutoSize = true;
            this.checkBoxCadence.Checked = true;
            this.checkBoxCadence.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCadence.Location = new System.Drawing.Point(153, 10);
            this.checkBoxCadence.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxCadence.Name = "checkBoxCadence";
            this.checkBoxCadence.Size = new System.Drawing.Size(69, 17);
            this.checkBoxCadence.TabIndex = 2;
            this.checkBoxCadence.Text = "Cadence";
            this.checkBoxCadence.UseVisualStyleBackColor = true;
            // 
            // checkBoxAltitude
            // 
            this.checkBoxAltitude.AutoSize = true;
            this.checkBoxAltitude.Checked = true;
            this.checkBoxAltitude.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAltitude.Location = new System.Drawing.Point(226, 10);
            this.checkBoxAltitude.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxAltitude.Name = "checkBoxAltitude";
            this.checkBoxAltitude.Size = new System.Drawing.Size(61, 17);
            this.checkBoxAltitude.TabIndex = 3;
            this.checkBoxAltitude.Text = "Altitude";
            this.checkBoxAltitude.UseVisualStyleBackColor = true;
            // 
            // checkBoxPower
            // 
            this.checkBoxPower.AutoSize = true;
            this.checkBoxPower.Checked = true;
            this.checkBoxPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPower.Location = new System.Drawing.Point(291, 10);
            this.checkBoxPower.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxPower.Name = "checkBoxPower";
            this.checkBoxPower.Size = new System.Drawing.Size(56, 17);
            this.checkBoxPower.TabIndex = 4;
            this.checkBoxPower.Text = "Power";
            this.checkBoxPower.UseVisualStyleBackColor = true;
            // 
            // Staticgraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 523);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "Staticgraph";
            this.Text = "Staticgraph";
            this.Load += new System.EventHandler(this.Staticgraph_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.CheckBox checkBoxHr;
        private System.Windows.Forms.CheckBox checkBoxSpeed;
        private System.Windows.Forms.CheckBox checkBoxCadence;
        private System.Windows.Forms.CheckBox checkBoxAltitude;
        private System.Windows.Forms.CheckBox checkBoxPower;
    }
}