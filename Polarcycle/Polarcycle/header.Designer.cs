﻿namespace Polarcycle
{
    partial class header
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(header));
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.staticGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataDetailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.staticGraphToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.title = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuStrip4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.menuStrip4);
            this.panel2.Location = new System.Drawing.Point(1, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1059, 81);
            this.panel2.TabIndex = 8;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(393, 18);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 44);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkRed;
            this.label1.Font = new System.Drawing.Font("Adobe Fan Heiti Std B", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(453, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 44);
            this.label1.TabIndex = 3;
            this.label1.Text = "Polar Cycle ";
            // 
            // menuStrip4
            // 
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.staticGraphToolStripMenuItem,
            this.dataDetailsToolStripMenuItem1,
            this.staticGraphToolStripMenuItem1});
            this.menuStrip4.Location = new System.Drawing.Point(0, 0);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(1059, 24);
            this.menuStrip4.TabIndex = 2;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // staticGraphToolStripMenuItem
            // 
            this.staticGraphToolStripMenuItem.Name = "staticGraphToolStripMenuItem";
            this.staticGraphToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.staticGraphToolStripMenuItem.Text = "File Summary";
            // 
            // dataDetailsToolStripMenuItem1
            // 
            this.dataDetailsToolStripMenuItem1.Name = "dataDetailsToolStripMenuItem1";
            this.dataDetailsToolStripMenuItem1.Size = new System.Drawing.Size(81, 20);
            this.dataDetailsToolStripMenuItem1.Text = "Data Details";
            // 
            // staticGraphToolStripMenuItem1
            // 
            this.staticGraphToolStripMenuItem1.Name = "staticGraphToolStripMenuItem1";
            this.staticGraphToolStripMenuItem1.Size = new System.Drawing.Size(83, 20);
            this.staticGraphToolStripMenuItem1.Text = "Static Graph";
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(441, 84);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(175, 29);
            this.title.TabIndex = 9;
            this.title.Text = "Text goes here";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1, 116);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1059, 382);
            this.panel1.TabIndex = 10;
            // 
            // header
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1061, 530);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.title);
            this.Controls.Add(this.panel2);
            this.Name = "header";
            this.Text = "Home";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem staticGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataDetailsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem staticGraphToolStripMenuItem1;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.Panel panel1;
    }
}